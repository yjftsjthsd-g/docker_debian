
latest: amd64
	docker build -t local/debian:latest -f Dockerfile --build-arg arch=amd64 .

amd64:
	docker build -t local/debian:amd64 -f Dockerfile --build-arg arch=amd64 .

i386:
	docker build -t local/debian:i386 -f Dockerfile --build-arg arch=i386 .

arm64:
	docker build -t local/debian:arm64 -f Dockerfile --build-arg arch=arm64 .

mips64el:
	docker build -t local/debian:mips64el -f Dockerfile --build-arg arch=mips64el .

all: latest i386 amd64 mips64el arm64

