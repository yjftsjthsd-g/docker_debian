FROM alpine:latest as build
RUN apk add -U debootstrap perl
RUN mkdir /target

ARG arch=amd64
ARG version=sid
RUN debootstrap --arch $arch $version /target http://deb.debian.org/debian/

FROM scratch as debianoutput
LABEL maintainer "Brian Cole <docker@brianecole.com>"
COPY --from=build /target /
CMD /bin/sh
