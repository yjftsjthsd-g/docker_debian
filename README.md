# Debian image

Uses debootstrap to install a vanilla debian into a container image.

WARNING: This image is not recommended for general use. I've only bothered with
sid, and there are outstanding bugs with security implications.


## Non-native architectures

Apparently, Docker is happy to play with qemu+binfmt to build and run non-native
containers; on a Debian-family build host, you can
```
sudo apt install qemu binfmt-support qemu-user-static
```
and then proceed as usual to run ex. arm64 containers. One oddity of this
arrangement is that the resulting images are still marked by Docker as "amd64"
and just contain non-x86 binaries; working out if/how to change this is a future
project.

Official Debian docs for assorted platforms:
https://wiki.debian.org/SupportedArchitectures
https://www.debian.org/ports/


## TODO

* Can/should I tell Docker that these are non-amd64 images? Not sure what the
  compatibility story is.
* Should I set CMD to BASH?
* Is there a config option I could inject to the final image to default to no-install-recommends?
    * yes: https://superuser.com/questions/615565/can-i-make-apt-get-always-use-no-install-recommends
    * could even make my own "-minimal" tags
* Is there a good way to install less in the target? (i.e. I do not need dmidecode in a container)
* Is there a good way to handle tagging all the variables? I'd like a full cross
  product of multiple architectures, debian versions, full/small/minimal
  installations.


## License

This repo is MIT (see LICENSE file), but note that that only covers the contents
of this repo (builds scripts, basically), not the contents of the generated
image!
